import React from 'react';
import {
  Breadcrumb,
  BreadcrumbItem,
  Button,
  Tabs,
  Tab,
  Grid,
  Column,
  OrderedList,
  ListItem,
  Form,
  TextArea,
  TextInput,
  Select,
  SelectItem,
} from '@carbon/react';
import { Chip32, Email16, LogoDiscord16 } from '@carbon/react/icons';
import { InfoSection, InfoCard } from '../../components/Info';
import Globe32 from '@carbon/icons-react/lib/globe/32';
import PersonFavorite32 from '@carbon/icons-react/lib/person--favorite/32';
import Application32 from '@carbon/icons-react/lib/application/32';

const props = {
  tabs: {
    selected: 0,
    role: 'navigation',
  },
  tab: {
    role: 'presentation',
    tabIndex: 0,
  },
};

const LandingPage = () => {
  return (
    <Grid className="landing-page">
      <Column lg={16} md={8} sm={4} className="landing-page__banner">
        <Breadcrumb noTrailingSlash aria-label="Page navigation">
          <BreadcrumbItem>
            <a href="/">Getting started</a>
          </BreadcrumbItem>
        </Breadcrumb>
        <h1 className="landing-page__heading">
          Design &amp; build with Carbon
        </h1>
      </Column>
      <Column lg={16} md={8} sm={4} className="landing-page__r2">
        <Tabs
          {...props.tabs}
          aria-label="Tab navigation"
          className="tabs-group">
          <Tab {...props.tab} label="About">
            <Grid className="tabs-group-content">
              <Column
                md={4}
                lg={7}
                sm={4}
                className="landing-page__tab-content">
                <h2 className="landing-page__subheading">About Me</h2>
                <p className="landing-page__p">
                  Dolor takimata duo lorem in sadipscing dolor labore erat
                  volutpat sanctus facilisi sea tempor eos dolor quod ut duis.
                  Nonumy lorem consequat erat sea gubergren ea. Dolor ea vero
                  tempor ullamcorper elitr invidunt justo dolor et. Enim et et
                  sea ea nonummy sea ipsum sed sit lorem eu sadipscing sed magna
                  amet tempor sanctus dolore. Autem sea diam magna diam
                  sadipscing consequat invidunt dolores enim tempor. Dolor no ut
                  est takimata accumsan sit sed et no feugait consetetur odio.
                  Magna at laoreet sit erat stet. Rebum eleifend tempor dolore
                  sit vero sanctus labore et ut. Vel dolores rebum ut congue at
                  tempor quis vulputate sed. Magna consectetuer delenit erat
                  stet clita duo diam accusam amet vero vero in no.
                </p>
                <Button>Learn more</Button>
              </Column>
              <Column md={4} lg={{ span: 8, offset: 7 }} sm={4}>
                <img
                  className="landing-page__illo"
                  src={`${process.env.PUBLIC_URL}/tab-illo.png`}
                  alt="Carbon illustration"
                />
              </Column>
            </Grid>
          </Tab>
          <Tab {...props.tab} label="Contact ME">
            <Grid className="tabs-group-content">
              <Column
                lg={16}
                md={8}
                sm={4}
                className="landing-page__tab-content">
                <div
                  style={{
                    display: 'contents',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '30vh',
                    margin: '20',
                  }}>
                  <h1>Ways to contact</h1>
                  <OrderedList>
                    <ListItem>
                      <Email16 />
                      Mail: sisit@dotmc.tk
                    </ListItem>
                    <ListItem>
                      <LogoDiscord16 /> Discord: SomePerson#1508
                    </ListItem>
                    <ListItem>Element matrix ID: @sisit135:matrix.org</ListItem>
                    <ListItem>Youtube: ####</ListItem>
                    <ListItem>Github: ####</ListItem>
                    <ListItem>Roblox #1: ####</ListItem>
                    <ListItem>Roblox #2: ####</ListItem>

                    <ListItem>Minecraft Java: ####</ListItem>
                    <ListItem>Minecraft BDX: ####</ListItem>
                  </OrderedList>
                </div>
                <div
                  style={{
                    display: 'table',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100vh',
                    width: '120vh',
                  }}>
                  <h1>or fill out this...</h1>
                  <Form name="contact" method="POST" data-netlify="true">
                    <div style={{ marginBottom: '2rem' }}>
                      <TextInput
                        helperText="Optional helper text here; if message is more than one line text should wrap (~100 character count maximum)"
                        id="test2"
                        invalidText="Invalid error message."
                        labelText="Text input label"
                        placeholder="name@example.com"
                        type="email"
                      />
                    </div>
                    <div style={{ marginBottom: '2rem' }}>
                      <TextArea
                        cols={50}
                        helperText="Optional helper text here; if message is more than one line text should wrap (~100 character count maximum)"
                        id="test5"
                        invalidText="Invalid error message."
                        labelText="Your message"
                        placeholder="Text here"
                        rows={4}
                      />
                    </div>
                    <div style={{ marginBottom: '2rem' }}>
                      <Select
                        defaultValue="placeholder-item"
                        id="select-1"
                        name="select1[]"
                        invalidText="This is an invalid error message."
                        labelText="Select">
                        <SelectItem text="Option 1" value="option-1" />
                        <SelectItem text="Option 2" value="option-2" />
                        <SelectItem text="Option 3" value="option-3" />
                      </Select>
                    </div>
                    <Button kind="primary" tabIndex={0} type="submit">
                      Submit
                    </Button>
                  </Form>
                </div>
              </Column>
            </Grid>
          </Tab>
          <Tab {...props.tab} label="PC Spec">
            <Grid className="tabs-group-content">
              <Column
                lg={16}
                md={8}
                sm={4}
                className="landing-page__tab-content">
                <InfoSection heading=" This is my PCs">
                  <InfoCard
                    heading="Desktop"
                    body="2 Socket Xeon E5 8C8T | 18G ECC Ram | Redeon RX 5500 4Gb | Geforce 950 4Gb (temp diabled due to PSU problem)."
                    icon={<Chip32 />}
                  />
                  <InfoCard
                    heading="Laptop #1"
                    body="(use for write with pen): Ryzen 4700U 8C8T | 16GB | Redeon Vega 8"
                    icon={<Chip32 />}
                  />
                  <InfoCard
                    heading="Notebook #2"
                    body="NB2: (old, not used): i7-6700HQ 4C8T | 8GB Ram |HD graphic 530 | Old geforce??"
                    icon={<Chip32 />}
                  />
                </InfoSection>
              </Column>
            </Grid>
          </Tab>
        </Tabs>
      </Column>
      <Column lg={16} md={8} sm={4}>
        <InfoSection heading="The Principles" className="landing-page__r3">
          <InfoCard
            heading="Carbon is Open"
            body="It's a distributed effort, guided by the principles of the open-source movement. Carbon's users are also it's makers, and everyone is encouraged to contribute."
            icon={<PersonFavorite32 />}
          />
          <InfoCard
            heading="Carbon is Modular"
            body="Carbon's modularity ensures maximum flexibility in execution. It's components are designed to work seamlessly with each other, in whichever combination suits the needs of the user."
            icon={<Application32 />}
          />
          <InfoCard
            heading="Carbon is Consistent"
            body="Based on the comprehensive IBM Design Language, every element and component of Carbon was designed from the ground up to work elegantly together to ensure consistent, cohesive user experiences."
            icon={<Globe32 />}
          />
        </InfoSection>
      </Column>
    </Grid>
  );
};

export default LandingPage;
