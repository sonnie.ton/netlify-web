import React from 'react';
import openInNewTab from '../../utills';
import {
  Header,
  HeaderContainer,
  HeaderName,
  HeaderNavigation,
  HeaderMenuButton,
  HeaderMenuItem,
  HeaderGlobalBar,
  HeaderGlobalAction,
  SkipToContent,
  SideNav,
  SideNavItems,
  HeaderSideNavItems,
} from '@carbon/react';
import {
  AppSwitcher20,
  Notification20,
  Search20,
  ChatLaunch20,
} from '@carbon/react/icons';
import { Link } from 'react-router-dom';

const TutorialHeader = () => (
  <HeaderContainer
    render={({ isSideNavExpanded, onClickSideNavExpand }) => (
      <Header aria-label="Carbon Tutorial">
        <SkipToContent />
        <HeaderMenuButton
          aria-label="Open menu"
          onClick={onClickSideNavExpand}
          isActive={isSideNavExpanded}
        />
        <HeaderName element={Link} to="/" prefix="S4TTK">
          Persornal web
        </HeaderName>
        <HeaderNavigation aria-label="Carbon Tutorial">
          <HeaderMenuItem element={Link} to="/repos">
            Repositories
          </HeaderMenuItem>
          <HeaderMenuItem element={Link} to="/softwares">
            Softwares
          </HeaderMenuItem>
          <HeaderMenuItem element={Link} to="/tips">
            Tips and trick
          </HeaderMenuItem>
        </HeaderNavigation>
        <SideNav
          aria-label="Side navigation"
          expanded={isSideNavExpanded}
          isPersistent={false}>
          <SideNavItems>
            <HeaderSideNavItems>
              <HeaderMenuItem element={Link} to="/repos">
                Repositories
              </HeaderMenuItem>
              <HeaderMenuItem element={Link} to="/softwares">
                Softwares
              </HeaderMenuItem>
              <HeaderMenuItem element={Link} to="/tips">
                Tips and trick
              </HeaderMenuItem>
            </HeaderSideNavItems>
          </SideNavItems>
        </SideNav>

        <HeaderGlobalBar>
          <HeaderGlobalAction
            aria-label="Search something on oogle"
            onClick={() => window.open('https://google.com', '_blank')}>
            <Search20 />
          </HeaderGlobalAction>
          <HeaderGlobalAction aria-label="Notifications">
            <Notification20 />
          </HeaderGlobalAction>
          <HeaderGlobalAction
            aria-label="Discord guild"
            onClick={() =>
              window.open('https://discord.gg/GDk7SeydHT', '_blank')
            }>
            <ChatLaunch20 />
          </HeaderGlobalAction>
          <HeaderGlobalAction aria-label="App Switcher">
            <AppSwitcher20 />
          </HeaderGlobalAction>
        </HeaderGlobalBar>
      </Header>
    )}
  />
);

export default TutorialHeader;
